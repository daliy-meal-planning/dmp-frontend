import 'package:flutter/material.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF1F8E9), // สีพื้นหลังเป็นสีเขียวอ่อนๆ
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'lib/images/logo.png', // ไฟล์รูปโลโก้ของแอปที่อยู่ในโฟลเดอร์ assets
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 20),
            Text(
              'CALCARB',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            SizedBox(height: 8),
            Text(
              'ห่วงใยสุขภาพของคุณกับเรา',
              style: TextStyle(
                fontSize: 16,
                color: Colors.black54,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        height: 80.0,
        width: 80.0,
        decoration: BoxDecoration(
          color: Color(0xFFF1F8E9), // สีพื้นหลังของปุ่ม
          shape: BoxShape.circle, // รูปร่างของปุ่ม
        ),
        child: IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: () {
            print('Next button pressed');
          },
        ),
      ),
    );
  }
}
