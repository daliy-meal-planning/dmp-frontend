import 'package:flutter/material.dart';

class Secondpage extends StatelessWidget {
  const Secondpage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.greenAccent.shade100,
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment:
                  MainAxisAlignment.center, // จัดตำแหน่งในแนวตั้ง
              crossAxisAlignment:
                  CrossAxisAlignment.center, // จัดตำแหน่งในแนวนอน
              children: [
                Image.asset(
                  'lib/images/food5.png', // เปลี่ยนเป็นพาธของรูปภาพของคุณ
                  height: 300, // ปรับขนาดของรูปภาพตามต้องการ
                  width: 300,
                ), // เพิ่มระยะห่างระหว่างรูปภาพและข้อความ
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    'การได้รับการจัดสรรสารอาหารอย่างเหมาะสม\nและสามารถเติมเต็มความอยากอาหารของคุณ\nด้วย CALCARB ของเรา\nที่จะจัดสรรเรื่องอาหารที่เหมาะสมกับ\nสุขภาพและความงามของคุณ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: Container(
          height: 80.0,
          width: 80.0,
          decoration: BoxDecoration(
            color: Colors.greenAccent.shade100, // สีพื้นหลังของปุ่ม
            shape: BoxShape.circle, // รูปร่างของปุ่ม
          ),
          child: IconButton(
            icon: Icon(Icons.arrow_forward),
            onPressed: () {
              print('Next button pressed');
            },
          ),
        ),
      ),
    );
  }
}
